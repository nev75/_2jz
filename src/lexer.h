#ifndef _2JZ_LEXER_H_INCLUDED_
#define _2JZ_LEXER_H_INCLUDED_

#define _POSIX_C_SOURCE 2

#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

typedef enum {
	TOK_NONE	= 0,
	TOK_DQUOTE,		/* "  34 */
	TOK_SQUOTE,		/* '  39 */
	TOK_LPAREN,		/* (  40 */
	TOK_RPAREN,		/* }  41 */
	TOK_COMMA,		/* ,  44 */
	TOK_SEMICOLON,		/* ;  59 */
	TOK_EQUAL,		/* =  61 */
	TOK_LSBRACKET,		/* [  91 */
	TOK_RSBRACKET,		/* ]  93 */
	TOK_LCBRACKET,		/* { 123 */
	TOK_VBAR,		/* | 124 */
	TOK_RCBRACKET,		/* } 125 */
	TOK_COMMENT,
	TOK_TERM,
	TOK_IDENT
} tokentype_t;

typedef struct {
	char		*s;
	tokentype_t	type;
	unsigned int	row;
	unsigned int	col;
} token_t;

char* lexer_tokentypes[TOK_IDENT + 1];

int lexer_init(const char *pathname);
void lexer_final(void);
void lexer_token_free(token_t *token);
token_t *lexer_get_next_token(void);

#endif /* _2JZ_LEXER_H_INCLUDED_ */
