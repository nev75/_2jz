/*
 * Copyright (C) 2018-2019 Eugene Nikolaev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "lexer.h"


#define LF	(unsigned char) '\n'
#define CR	(unsigned char) '\r'

#define BUFFER_FRAME 128


char* lexer_tokentypes[TOK_IDENT + 1] = {
	"TOK_NONE",
	"TOK_DQUOTE",
	"TOK_SQUOTE",
	"TOK_LPAREN",
	"TOK_RPAREN",
	"TOK_COMMA",
	"TOK_SEMICOLON",
	"TOK_EQUAL",
	"TOK_LSBRACKET",
	"TOK_RSBRACKET",
	"TOK_LCBRACKET",
	"TOK_VBAR",
	"TOK_RCBRACKET",
	"TOK_COMMENT",
	"TOK_TERM",
	"TOK_IDENT"
};


typedef struct {
	size_t		off;
	unsigned int	row;
	unsigned int	col;
} pos_t;


static pos_t pos;

static char *buffer = NULL;
static size_t buff_size = 0;


int lexer_init(const char *pathname)
{
	int res = -1;

	pos = (pos_t) { 0, 1, 1 };
	buff_size = 0;

	FILE *fp;
	if ((fp = fopen(pathname, "r")) != NULL) {
		struct stat sb;
		int fd = fileno(fp);
		if (!fstat(fd, &sb)) {
			buff_size = sb.st_size;
			buffer = malloc(buff_size);
			if (buffer) {
				if (fread(buffer, 1, buff_size, fp)) {
					res = 0;
				} else {
					free(buffer);
					buffer = NULL;
				}
			}
		}
		fclose(fp);
	}

	return res;
}

void lexer_final(void)
{
	free(buffer);
	buffer = NULL;
}

void lexer_token_free(token_t *token)
{
	if (token != NULL) {
		free(token->s);
	}
	free(token);
}

char lexer_peek(void)
{
	if (pos.off < buff_size) {
		return buffer[pos.off];
	} else {
		return '\0';
	}
}

char lexer_step(void)
{
	if (lexer_peek() == LF) {
		pos.row++;
		pos.col = 1;
	} else {
		pos.col++;
	}

	pos.off++;

	return lexer_peek();
}

int lexer_skip_whitespace(void)
{
	char ch = lexer_peek();

	while (ch != '\0') {
		if (isspace(ch)) {
			ch = lexer_step();
		} else {
			return 1;
		}
	}

	return 0;
}

char *lexer_get_comment(void)
{
	char *res = NULL;
	unsigned short res_size = 0;
	unsigned short mem_size = 0;

	pos_t saved_pos = pos;

	if (lexer_peek() == '(') {
		lexer_step();
		if (lexer_peek() == '*') {
			lexer_step();
			mem_size += BUFFER_FRAME;
			res = malloc(mem_size);
			/* TODO: check res */
			char ch;
			while ((ch = lexer_peek()) != '\0') {
				lexer_step();
				if (ch == ')' && res_size >= 1 && res[res_size-1] == '*') {
					res_size--;
					break;
				}
				if (res_size >= mem_size) {
					mem_size += BUFFER_FRAME;
					res = realloc(res, mem_size);
					/* TODO: check res */
				}
				res[res_size++] = ch;
			}
			res = realloc(res, res_size + 1);
			/* TODO: check res */
			res[res_size] = '\0';
		}
	}

	if (res == NULL) {
		pos = saved_pos;
	}

	return res;
}

/* TODO: add escape sequences */
char *lexer_get_terminal(void)
{
	char *res = NULL;
	unsigned short res_size = 0;
	unsigned short mem_size = 0;

	pos_t saved_pos = pos;

	char tch = lexer_peek();
	if (tch == '\'' || tch == '"') {
		lexer_step();
		mem_size += BUFFER_FRAME;
		res = malloc(mem_size);
		/* TODO: check res */
		char ch;
		while ((ch = lexer_peek()) != '\0') {
			lexer_step();
			if (ch == tch) {
				break;
			}
			if (res_size >= mem_size) {
				mem_size += BUFFER_FRAME;
				res = realloc(res, mem_size);
				/* TODO: check res */
			}
			res[res_size++] = ch;
		}
		res = realloc(res, res_size + 1);
		/* TODO: check res */
		res[res_size] = '\0';
	}

	if (res == NULL) {
		pos = saved_pos;
	}

	return res;
}

char *lexer_get_identifier(void)
{
	char *res = NULL;
	unsigned short res_size = 0;
	unsigned short mem_size = 0;

	char ch = lexer_peek();
	if (ch == '_' || isalpha(ch)) {
		lexer_step();
		mem_size += BUFFER_FRAME;
		res = malloc(mem_size);
		/* TODO: check res */
		res[res_size++] = ch;
		while ((ch = lexer_peek()) != '\0') {
			if (ch == '_' || isalnum(ch)) {
				lexer_step();
				if (res_size >= mem_size) {
					mem_size += BUFFER_FRAME;
					res = realloc(res, mem_size);
					/* TODO: check res */
				}
				res[res_size++] = ch;
			} else {
				break;
			}
		}
		res = realloc(res, res_size + 1);
		/* TODO: check res */
		res[res_size] = '\0';
	}

	return res;
}

/* TODO: move to helpers */
char *lexer_clone_str(const char *s)
{
	char *res = malloc(strlen(s) + 1);
	if (res != NULL) {
		strcpy(res, s);
	} else {
		/* TODO: raise error */
	}

	return res;
}

token_t *lexer_get_next_token(void)
{
	token_t *token = NULL;

	if (lexer_skip_whitespace()) {

		token = malloc(sizeof(token_t));
		if (token == NULL ){
			/* TODO: raise exception */
			return NULL;
		}

		token->s = NULL;
		token->type = TOK_NONE;
		token->row = pos.row;
		token->col = pos.col;

		char *s;
		char ch = lexer_peek();

		switch (ch) {

		case '=':
			token->s = lexer_clone_str("=");
			token->type = TOK_EQUAL;
			lexer_step();
			break;

		case '[':
			token->s = lexer_clone_str("[");
			token->type = TOK_LSBRACKET;
			lexer_step();
			break;

		case ']':
			token->s = lexer_clone_str("]");
			token->type = TOK_RSBRACKET;
			lexer_step();
			break;

		case '{':
			token->s = lexer_clone_str("{");
			token->type = TOK_LCBRACKET;
			lexer_step();
			break;

		case '}':
			token->s = lexer_clone_str("}");
			token->type = TOK_RCBRACKET;
			lexer_step();
			break;

		case '(':
			s = lexer_get_comment();
			if (s) {
				token->s = s;
				token->type = TOK_COMMENT;
			} else {
				token->s = lexer_clone_str("(");
				token->type = TOK_LPAREN;
				lexer_step();
			}
			break;

		case ')':
			token->s = lexer_clone_str(")");
			token->type = TOK_RPAREN;
			lexer_step();
			break;

		case '|':
			token->s = lexer_clone_str("|");
			token->type = TOK_VBAR;
			lexer_step();
			break;

		case ',':
			token->s = lexer_clone_str(",");
			token->type = TOK_COMMA;
			lexer_step();
			break;

		case '\'':
		case '"':
			s = lexer_get_terminal();
			if (s) {
				token->s = s;
				token->type = TOK_TERM;
			} else {
				printf("Unknowning char: '%c' at %d:%d\n", ch, pos.row, pos.col);
				lexer_step();
				free(token);
				token = NULL;
			}
			break;

		case ';':
			token->s = lexer_clone_str(";");
			token->type = TOK_SEMICOLON;
			lexer_step();
			break;

		default:
			s = lexer_get_identifier();
			if (s) {
				token->s = s;
				token->type = TOK_IDENT;
			} else {
				printf("Unknowning char: '%c' at %d:%d\n", ch, pos.row, pos.col);
				lexer_step();
				free(token);
				token = NULL;
			}
			break;
		}
	}

	return token;
}
