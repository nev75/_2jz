/*
 * Copyright (C) 2018-2019 Eugene Nikolaev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */


#include "ast.h"


char *ast_types[AST_TERM + 1] = {
	"AST_NONE",
	"AST_GRAMMAR",
	"AST_RULE",
	"AST_LHS",
	"AST_RHS",
	"AST_DEFINITION",
	"AST_OPTION",
	"AST_REPEAT",
	"AST_GROUP",
	"AST_IDENT",
	"AST_TERM"
};


ast_t *ast_init(ast_type_t type)
{	
	ast_t *res = malloc(sizeof(ast_t));
	/* TODO: check res */
	res->type = type;
	res->token = NULL;
	res->used = 0;
	res->size = 0;
	res->children = NULL;
	return res;
}

int ast_add(ast_t *self, ast_t *child)
{
	if (!self || !child) {
		return 1;
	}

	if (++self->used > self->size) {
		self->size += AST_LIST_RESERVE_CNT;
		/* TODO: self->size += max(AST_LIST_PRESERVE_CNT, (self->used - self->size)); */
		if (self->children == NULL) {
			self->children = calloc(self->size, sizeof(child));
			/* TODO: check self->children */
		} else {
			self->children = realloc(self->children, self->size * sizeof(child));
			/* TODO: check self->children */
		}
	}

	self->children[self->used - 1] = child;

	return 0;
}

void ast_free(ast_t *self)
{
	if (self != NULL) {
		int i = self->used;
		while (i > 0) {
			ast_free(self->children[--i]);
		}
		free(self->children);
		self->children = NULL;
		self->used = self->size = 0;
		lexer_token_free(self->token);
		self->type = AST_NONE;
	}
	free(self);
}
