#ifndef _2JZ_AST_H_INCLUDED_
#define _2JZ_AST_H_INCLUDED_


#include <stdlib.h>
#include "lexer.h"


#define AST_LIST_RESERVE_CNT 5


typedef enum {
	AST_NONE	= 0,
	AST_GRAMMAR,
	AST_RULE,
	AST_LHS,
	AST_RHS,
	AST_DEFINITION,
	AST_OPTION,
	AST_REPEAT,
	AST_GROUP,
	AST_IDENT,
	AST_TERM
} ast_type_t;

typedef struct ast_s ast_t;

struct ast_s {
	ast_type_t	type;
	token_t		*token;
	int		used;
	int		size;
	ast_t		**children;
};


char* ast_types[AST_TERM + 1];


ast_t *ast_init(ast_type_t type);
int ast_add(ast_t *self, ast_t *child);
void ast_free(ast_t *self);

#endif /* _2JZ_AST_H_INCLUDED_ */
