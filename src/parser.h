#ifndef _2JZ_PARSER_H_INCLUDED_
#define _2JZ_PARSER_H_INCLUDED_

#include "ast.h"

ast_t *parser_parse(const char *pathname);

#endif /* _2JZ_PARSER_H_INCLUDED_ */
