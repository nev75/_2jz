/*
 * Copyright (C) 2018-2019 Eugene Nikolaev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "main.h"
#include "parser.h"

static const char *grammar_filename = "./conf/language.gr";

void help(const char *filename)
{
	fprintf(stderr, "Usage: %s [-h] [-f grammar_filename]\n", filename);
}

int args_parse(int argc, char *argv[])
{
	int res = 0, opt;
	while ((opt = getopt(argc, argv, "hf:")) != -1) {
		switch (opt) {
		case 'h':
			help(argv[0]);
			exit(EXIT_SUCCESS);
		case 'f':
			grammar_filename = optarg;
			break;
		default:
			help(argv[0]);
			res = -1;
			break;
		}
	}
	return res;
}

void print_ast(ast_t *ast, int indent)
{
	if (ast == NULL) {
		return;
	}

	printf("%*s", indent * 4, "");
	printf("ast: %s size %d/%d",
		ast_types[ast->type], ast->used, ast->size);
	token_t *token = ast->token;
	if (token != NULL) {
		printf("; token: %s \"%s\" at %d:%d",
			lexer_tokentypes[token->type],
			token->s, token->row, token->col);
	}
	printf("\n");

	int i = 0;
	while (i < ast->used) {
		print_ast(ast->children[i++], indent + 1);
	}
}

int main(int argc, char *argv[])
{
	if (args_parse(argc, argv) != 0) {
		exit(EXIT_FAILURE);
	}

	ast_t *grammar = parser_parse(grammar_filename);
	if (grammar != NULL) {
		print_ast(grammar, 0);
		ast_free(grammar);
	} else {
		/* TODO: move message to the file opener place */
		fprintf(stderr, "Unable to find the language grammar file: '%s'\n", grammar_filename);
		/* TODO: raise exception */
		return 1;
	}

	return 0;
}
