/*
 * Copyright (C) 2018-2019 Eugene Nikolaev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */


#include "lexer.h"
#include "parser.h"


static token_t *curr_token;

ast_t *parser_rhs(ast_type_t type);


void parser_error(void)
{
	if (curr_token != NULL) {
		fprintf(stderr, "Invalid syntax at %d:%d\n",
			curr_token->row, curr_token->col);
	} else {
		fprintf(stderr, "Unexpected end of file\n");
	}

	exit(EXIT_FAILURE);
}

void parser_whitespace(void)
{
	while (curr_token != NULL) {
		if (curr_token->type == TOK_COMMENT) {
			lexer_token_free(curr_token);
			curr_token = lexer_get_next_token();
		} else {
			break;
		}
	}
}

void parser_step(tokentype_t type, int keep)
{
	parser_whitespace();

	if (curr_token != NULL && curr_token->type == type) {
		if (!keep) {
			lexer_token_free(curr_token);
		}
		curr_token = lexer_get_next_token();
	} else {
		parser_error();
	}
}

ast_t *parser_identifier(void)
{
	ast_t *ident = ast_init(AST_IDENT);
	ident->token = curr_token;
	
	parser_step(TOK_IDENT, 1);

	return ident;
}

ast_t *parser_terminal(void)
{
	ast_t *term = ast_init(AST_TERM);
	term->token = curr_token;
	
	parser_step(TOK_TERM, 1);

	return term;
}

ast_t *parser_lhs(void)
{
	ast_t *lhs = ast_init(AST_LHS);
	ast_add(lhs, parser_identifier());

	return lhs;
}

ast_t *parser_primary(void)
{
	ast_t *primary = NULL;

	switch (curr_token->type) {
	
	case TOK_IDENT:
		primary = parser_identifier();
		break;

	case TOK_TERM:
		primary = parser_terminal();
		break;

	case TOK_LSBRACKET:
		parser_step(TOK_LSBRACKET, 0);
		primary = ast_init(AST_OPTION);
		ast_add(primary, parser_rhs(AST_RHS));
		parser_step(TOK_RSBRACKET, 0);
		break;

	case TOK_LCBRACKET:
		parser_step(TOK_LCBRACKET, 0);
		primary = ast_init(AST_REPEAT);
		ast_add(primary, parser_rhs(AST_RHS));
		parser_step(TOK_RCBRACKET, 0);
		break;

	case TOK_LPAREN:
		parser_step(TOK_LPAREN, 0);
		primary = ast_init(AST_GROUP);
		ast_add(primary, parser_rhs(AST_RHS));
		parser_step(TOK_RPAREN, 0);
		break;

	default:
		break;
	}

	if (primary == NULL) {
		parser_error();
	}

	return primary;
}

ast_t *parser_definition(void)
{
	ast_t *def = ast_init(AST_DEFINITION);

	while (1) {
		ast_add(def, parser_primary());
		if (curr_token->type != TOK_COMMA) {
			break;
		}
		parser_step(TOK_COMMA, 0);
	}

	return def;
}

ast_t *parser_rhs(ast_type_t type)
{
	ast_t *rhs = ast_init(type);

	while (1) {
		ast_add(rhs, parser_definition());
		if (curr_token->type != TOK_VBAR) {
			break;
		}
		parser_step(TOK_VBAR, 0);
	}

	return rhs;
}

ast_t *parser_rule(void)
{
	ast_t *lhs = parser_lhs();
	parser_step(TOK_EQUAL, 0);
	ast_t *rhs = parser_rhs(AST_RHS);
	parser_step(TOK_SEMICOLON, 0);

	ast_t *rule = ast_init(AST_RULE);
	ast_add(rule, lhs);
	ast_add(rule, rhs);

	return rule;
}

ast_t *parser_grammar(void)
{
	ast_t *grammar = ast_init(AST_GRAMMAR);

	while (curr_token != NULL) {
		ast_add(grammar, parser_rule());
	}

	return grammar;
}

ast_t *parser_parse(const char *pathname)
{
	ast_t *node = NULL;

	if (!lexer_init(pathname)) {
		curr_token = lexer_get_next_token();
		parser_whitespace();
		node = parser_grammar();
		lexer_final();
	}

	return node;
}
